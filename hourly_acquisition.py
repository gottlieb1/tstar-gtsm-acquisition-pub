import os
import logging
import datetime
import psycopg2
import psycopg2.extras
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as dates
from fabric import Connection
from paramiko import ssh_exception
import miniseed_translator

def get_logger():
    LOGGER = logging.getLogger("hourly_acquisition_logger")
    logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s', level=os.environ.get("LOGLEVEL", "INFO"))
    return LOGGER

def connect_to_postgres(dbname, user, host, password):
    try:
        conn = psycopg2.connect(dbname=dbname, user=user, host=host, password=password)
        cur = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        return conn, cur
    except Exception as e:
        LOGGER.error("Failed to connect to the database")
        exit(1)

def read_config_table(cur):

    SQL_QUERY = """select * from gtsm_configs"""
    try:
        cur.execute(SQL_QUERY)
    except:
        LOGGER.error("Failed on query %s", SQL_QUERY)
    configs = cur.fetchall()
    return configs

def create_ssh_connection(config):
    user = config['username']
    host = config['fqdn']
    keyfilename = '/app/.keys/' + config['key']
    port = config['port']
    LOGGER.debug(host,user,port,keyfilename)
    c = Connection(host=host, user=user, port=port, connect_kwargs={"key_filename": keyfilename})
    return c


def get_logger_file_list(config):
    fcid = config['fcid']
    LOGGER.info("Getting list of files at %s", fcid)
    ssh_connection=create_ssh_connection(config)
    dirs = ['/data/Day', '/data/Hour', '/data/Min']
    logger_files = []
    try:
        for dir in dirs:
            cmd = "find " + dir + " -type f"
            files = ssh_connection.run(cmd, hide=True).stdout.split('\n')
            for file in files:
                if file.split('/')[-1][0:4] == fcid:  # filter filepath for expected site fcid only
                    if (dir == '/data/Day' or dir == '/data/Hour') and file[-4:] == '.tgz':
                        logger_files.append(file)
                    elif dir == '/data/Min' and file[-6:] == '20.tar':  # filter 100hz or incomplete tgzs
                        logger_files.append(file)
        return logger_files
    except (ssh_exception.NoValidConnectionsError, TimeoutError) as e:
        LOGGER.error(e)
        return []

def get_db_file_list(conn, fcid):
    try:
        cur=conn.cursor()
        SQL = """
                SELECT filename
                FROM dataflow_tracking
                WHERE fcid = %s
              """
        SQLdata = (fcid,)
        cur.execute(SQL, SQLdata)
        db_files = [r[0] for r in cur.fetchall()]
        return db_files
    except:
        LOGGER.error("Failed on query %s %s", SQL, SQLdata)

def download_file_from_logger(ssh_connection, filename, local_path):

    try:
        result=ssh_connection.get(filename, local_path+filename)
        return True
    except Exception as e:
        LOGGER.error(e)
        os.remove(local_path)   
        return False

def parse_filename(filename):
    file_info = {}
    file_info['filename'] = filename
    file_info['file'] = file_info['filename'].split('/')[-1]
    file_info['fcid'] = file_info['file'][0:4]
    file_info['yr'] = file_info['file'][4:6]
    file_info['doy'] = file_info['file'][6:9]
    file_info['date'] = datetime.datetime.strptime(file_info['yr']+file_info['doy'], '%y%j')
    file_info['mn'] = file_info['date'].strftime('%m')
    file_info['day'] = file_info['date'].strftime('%d')
    if file_info['file'][-7:] == 'Day.tgz':
        file_info['type'] = 'Day'
    elif file_info['file'][-6:] == '20.tar':
        file_info['type'] = 'Min'
    elif file_info['file'][-4:] == '.tgz':
        file_info['type'] = 'Hour'
    return file_info


def mark_as_downloaded(conn,cur,file_info):
    time = datetime.datetime.now(datetime.timezone.utc).strftime("%Y-%m-%dT%H:%M:%S%Z")
    LOGGER.info("file downloaded at %s", time)
    try:
        SQL = """
                             INSERT INTO dataflow_tracking (filename, fcid, downloaded, download_time, file_type, file_date)
                             VALUES (%s, %s, %s, %s, %s, %s)
                          """
        SQLdata = (file_info['filename'], file_info['fcid'], 'true', time, file_info['type'], file_info['date'])
        cur.execute(SQL, SQLdata)
        conn.commit()
    except psycopg2.errors.UniqueViolation:
        LOGGER.error("%s already marked as downloaded", file_info['filename'])
    except Exception as e:
        LOGGER.exception(e)

def get_download_stats(conn):
    try:
        SQL = """
                            select fcid, file_date, file_type, count(*) 
                            from dataflow_tracking
                            group by fcid ,file_date, file_type
                            order by file_date asc;
                          """
        df = pd.read_sql_query(SQL, conn)
        return df

    except Exception as e:
        LOGGER.error(e)
        LOGGER.error("Failed on query %s %s", SQL)

def get_last_connection(conn, fcid):
    cur=conn.cursor()
    SQL = """
                            select download_time
                            from dataflow_tracking
                            where fcid = %s
                            order by download_time desc
                            limit 1
                          """
    SQLdata = (fcid,)
    cur.execute(SQL, SQLdata)
    last_connection = cur.fetchall()[0][0].strftime('%Y-%m-%d %H:%m')
    return last_connection


def plot_download_history(fcid, df, last_connection):
    x_max = datetime.datetime.utcnow().date()
    x_min = x_max - datetime.timedelta(days=10)

    df = df.set_index('file_date')
    df = df[df.index >= x_min]
    data = pd.DataFrame(index=df.index.unique())
    data['Day'] = df[df['file_type'] == "Day"]['count']
    data['Hour'] = df[df['file_type'] == "Hour"]['count']
    data['Min'] = df[df['file_type'] == "Min"]['count']
    data = data.fillna(0)
    fig, ax = plt.subplots(figsize=(12, 8))
    ax.bar(data.index, data.Day, color='mediumblue', width=0.5, label="low rate")
    ax.bar(data.index, data.Hour, bottom=data.Day, color='cornflowerblue', width=0.5, label="1 hz")
    ax.bar(data.index, data.Min, bottom=data.Day + data.Hour, color='lightsteelblue', width=0.5, label="20 hz")
    ax.xaxis.set_major_locator(dates.DayLocator(interval=1))
    ax.set_title(fcid + " 10 day download history.  Last connection at " + last_connection + " UTC")
    ax.set_ylabel("Files downloaded")
    ax.legend()
    plt.xticks(rotation=90)
    plt.savefig("/app/data/"+fcid +"/"+fcid+ "_10_day_download_history.png")

def plot_all_downloads(fcid, df):
    df = df.set_index('file_date')
    data = pd.DataFrame(index=df.index.unique())
    data['low_rate'] = df[df['file_type'] == "Day"]['count']
    data['one_hz'] = df[df['file_type'] == "Hour"]['count']/24
    data['twenty_hz'] = df[df['file_type'] == "Min"]['count']/24
    #filter out old day 366 data from B012
    data=data[data.index > datetime.date.fromisoformat("2021-02-23")]
    data_start=data.index[0]
    data = data.fillna(0)
    means=round(data.mean(),3)*100
    fig, axs = plt.subplots(3, sharex=True, figsize=(12, 8))
    fig.suptitle(fcid + " lifetime downloads", fontsize=16)
    file_types=["low_rate","one_hz","twenty_hz"]
    colors=['mediumblue','cornflowerblue','lightsteelblue']
    for i, ax in enumerate(axs):
        ax.scatter(data.index, data[file_types[i]], color=colors[i], label="Percent collected "+str(means[i]))
        ax.xaxis.set_major_locator(dates.MonthLocator(interval=1))
        ax.set_ylabel("Percent complete")
        ax.legend(loc='lower left')
        ax.set_title(file_types[i])
        ax.set_ylim(0,1.1)
    plt.xticks(rotation=90)
    plt.savefig("/app/data/" + fcid + "/" + fcid + "_lifetime_history.png")

if __name__ == '__main__':

    LOGGER = get_logger()

    dbname = os.getenv('PG_DB_NAME')
    user = os.getenv('PG_DB_USER')
    host = os.getenv('PG_DB_HOST')
    password = os.getenv('PG_DB_PW')

    conn, cur = connect_to_postgres(dbname, user, host, password)

    configs = read_config_table(cur)

    for config in configs:
        fcid = config['fcid']
        if config['download']:
            ssh_connection = create_ssh_connection(config)
            LOGGER.info("Downloads enabled for %s", fcid)

            logger_files = get_logger_file_list(config)
            db_files = get_db_file_list(conn, fcid)

            for filename in logger_files:
                # FOR TESTING INCLUDES FLAG TO ONLY DOWNLOAD RECENT DATA (starting 2022 day 66), REMOVE FOR ACTUAL USE
                #if filename not in db_files and (int(filename.split('/')[-1][4:9]) > 22066):
                if filename not in db_files:
                    LOGGER.info("downloading %s", filename)
                    local_path = "/app/data/" + fcid
                    success = download_file_from_logger(ssh_connection, filename, local_path)
                    if success:
                        file_info = parse_filename(filename)
                        mark_as_downloaded(conn, cur, file_info)
                        miniseed_translator.translate(local_path, filename, file_info['type'])

            try:
                stats = get_download_stats(conn)
                last_connection = get_last_connection(conn, fcid)
                plot_download_history(fcid, stats[stats['fcid'] == fcid], last_connection)
                plot_all_downloads(fcid, stats[stats['fcid'] == fcid])
            except Exception as e:
                LOGGER.exception(e)

    cur.close()
    conn.close()
    LOGGER.info("Process complete.")


