import os, sys
import obspy


fcid = sys.argv[1]

dir='data/'+fcid+'/data/miniseed/'
miniseed_files=os.listdir(dir)
st=obspy.core.stream.Stream()
for file in miniseed_files:
    st+=obspy.core.stream.read(dir+file)
st.merge()
print(st.__str__(extended=True))

