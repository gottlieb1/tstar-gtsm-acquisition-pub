import os, sys, logging
import tarfile
import numpy as np
import obspy
import datetime
import bottle
import psycopg2
import psycopg2.extras



LOGGER = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s', level=os.environ.get("LOGLEVEL", "INFO"))


def connect_to_postgres():

    dbname = os.getenv('PG_DB_NAME')
    user = os.getenv('PG_DB_USER')
    host = os.getenv('PG_DB_HOST')
    password = os.getenv('PG_DB_PW')
    try:
        conn = psycopg2.connect(dbname=dbname, user=user, host=host, password=password)
    except:
        LOGGER.error("Failed to connect to the database")
        exit(1)
    cur = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
    return conn, cur

def get_network_code(cur, fcid):

    SQL_QUERY = """select network_code from gtsm_configs where fcid = %s"""
    SQL_DATA = (fcid,)
    try:
        cur.execute(SQL_QUERY, SQL_DATA)
    except:
        LOGGER.error("Failed on query %s %s", SQL_QUERY, SQL_DATA)
    network_code = cur.fetchall()[0]['network_code']
    return network_code


def get_channel_and_location(cur, channel_name, file_type):

    SQL_QUERY = """
                    select seed_channel, seed_location 
                    from bsm_data_type
                    where bottle_name = %s and file_type = %s
                """
    SQL_DATA = (channel_name,file_type)
    try:
        cur.execute(SQL_QUERY, SQL_DATA)
    except:
        LOGGER.error("Failed on query %s %s", SQL_QUERY, SQL_DATA)
    results = cur.fetchall()[0]
    return results['seed_channel'], results['seed_location']


def bottle2miniseed(bottlefile, file_type, cur):
    try:
        #LOGGER.info(bottlefile)
        btl = bottle.Bottle(bottlefile)
        stats = obspy.core.trace.Stats()
        (stats.station, channel, year, dayofyear, hour, min) = btl.parse_filename()
        metadata = btl.read_header()
        stats.delta = metadata['interval']
        stats.npts = metadata['num_pts']
        stats.starttime += datetime.timedelta(seconds=metadata['start'])
        stats.network = get_network_code(cur, stats.station)
        stats.channel, stats.location = get_channel_and_location(cur, channel, file_type)

        data = np.array(btl.read_data())
        tr = obspy.core.trace.Trace(data=data, header=stats)

        miniseed_dir = "../data/miniseed/"
        if file_type == "Min":
            #write minute long miniseed in temp directory to merge later
            miniseed_dir = ""
        miniseed_filename = miniseed_dir + bottlefile + ".ms." + stats.channel + "." + stats.location
        tr.write(miniseed_filename, format="MSEED")
        return miniseed_filename
    except Exception as e:
        LOGGER.exception(e)
        return False

def mark_as_translated(conn, cur, filepath):
    time = datetime.datetime.now(datetime.timezone.utc).strftime("%Y-%m-%dT%H:%M:%S%Z")
    LOGGER.info("file translated at %s", time)
    try:
        SQL = """    UPDATE dataflow_tracking 
                     SET mseed_conversion = %s, 
                     mseed_conversion_time = %s
                     WHERE filename = %s
              """
        SQLdata = ('true', time, filepath)
        cur.execute(SQL, SQLdata)
        #print(SQL, SQLdata)
        conn.commit()
    except Exception as e:
        LOGGER.exception(e)

def translate_hour_file(local_path, filepath):
    work_dir = local_path + '/temp/'
    os.chdir(work_dir)
    file_type = "Hour"
    conn, cur = connect_to_postgres()
    try:
        tar = tarfile.open("../" + filepath, "r:gz")

        for tarinfo in tar:
            tar.extract(tarinfo)
            bottlenames = tar.getnames()
        tar.close()
    except:
        LOGGER.error("Trying to untar %s: %s" % (filepath, sys.exc_info()[1]))
        cur.close()
        conn.close()
        return False

    successful_translations = 0
    for num_bottles, bottlefile in enumerate(bottlenames, start=1):
        miniseed_filename = bottle2miniseed(bottlefile, file_type, cur)
        if miniseed_filename:
            successful_translations +=1
        os.remove(bottlefile)
    if successful_translations == num_bottles:
        mark_as_translated(conn, cur, filepath)
        cur.close()
        conn.close()
        return True
    else:
        LOGGER.error("Only successfully translated %i of %i bottle files", successful_translations, num_bottles)
        cur.close()
        conn.close()
        return False

def translate_day_file(local_path, filepath):
    work_dir = local_path + '/temp/'
    os.chdir(work_dir)
    file_type = "Day"
    conn, cur = connect_to_postgres()
    try:
        tar = tarfile.open("../" + filepath, "r:gz")

        for tarinfo in tar:
            tar.extract(tarinfo)
            bottlenames = tar.getnames()
        tar.close()
    except:
        LOGGER.error("Trying to untar %s: %s" % (filepath, sys.exc_info()[1]))
        cur.close()
        conn.close()
        return False

    successful_translations = 0
    for num_bottles, bottlefile in enumerate(bottlenames, start=1):
        miniseed_filename = bottle2miniseed(bottlefile, file_type, cur)
        if miniseed_filename:
            successful_translations +=1
        os.remove(bottlefile)
    if successful_translations == num_bottles:
        mark_as_translated(conn, cur, filepath)
        cur.close()
        conn.close()
        return True
    else:
        LOGGER.error("Only successfully translated %i of %i bottle files", successful_translations, num_bottles)
        cur.close()
        conn.close()
        return False

def translate_min_file(local_path, filepath):
    work_dir = local_path + '/temp/'
    os.chdir(work_dir)
    file_type = "Min"
    conn, cur = connect_to_postgres()
    try:
        tar = tarfile.open("../" + filepath, "r:")

        minfilenames = tar.getnames()
        tar.extractall()
        tar.close()
        bottlenamelist = []
        for mfname in minfilenames:
            tar = tarfile.open(mfname, "r:gz")
            bottlenamelist.append(tar.getnames())
            tar.extractall()
            tar.close()
            os.remove(mfname)
        bottlenamelist = sorted(bottlenamelist)
    except:
        LOGGER.error("Trying to untar %s: %s" % (filepath, sys.exc_info()[1]))
        cur.close()
        conn.close()
        return False

    successful_translations = 0
    num_bottles = 0
    miniseed_files=[]
    bottle_root = bottlenamelist[0][0][0:11]
    for bottlenames in bottlenamelist:
        for bottlefile in bottlenames:
            num_bottles += 1
            miniseed_filename = bottle2miniseed(bottlefile, file_type, cur)
            if miniseed_filename:
                successful_translations +=1
                miniseed_files.append(miniseed_filename)
            os.remove(bottlefile)
    if successful_translations == num_bottles:
        merge_minute_files(miniseed_files, bottle_root)
        mark_as_translated(conn, cur, filepath)
        cur.close()
        conn.close()
        return True
    else:
        LOGGER.error("Only successfully translated %i of %i bottle files", successful_translations, num_bottles)
        cur.close()
        conn.close()
        return False

def merge_minute_files(miniseed_files, bottle_root):
    channels = {'BS1':'CH0', 'BS2':'CH1','BS3':'CH2','BS4':'CH3'}
    try:
        st = obspy.core.stream.Stream()
        for file in miniseed_files:
            st += obspy.core.stream.read(file)
        st.merge(fill_value=999999) #handle gaps in data by filling 999999s, the standard null value for gtsm
        for tr in st:
            bottlefile = bottle_root + channels[tr.stats.channel] + '_20'
            miniseed_dir = "../data/miniseed/"
            miniseed_filename = miniseed_dir + bottlefile + ".ms." + tr.stats.channel + "." + tr.stats.location
            tr.write(miniseed_filename, format="MSEED")
        for file in miniseed_files:
            os.remove(file)
    except Exception as e:
        LOGGER.exception(e)


def translate(local_path, filepath, file_type):
    #entrypoint for hourly_acquisition script
    #local path in form of '/app/data/fcid
    #filepath in form of '/data/Day/bottlename.tgz'

    LOGGER.info("translating " + filepath)

    if file_type == 'Min':
        translate_min_file(local_path, filepath)
    elif file_type == 'Day':
        translate_day_file(local_path, filepath)
    elif file_type == 'Hour':
        translate_hour_file(local_path, filepath)
    else:
        LOGGER.error("file type missing or invalid.")

if __name__ == '__main__':

    #if invoke directly, use filepath relative to current directory ie 'data/fcid/data/Day/bottlename.tgz'
    os.environ["PG_DB_NAME"] = "tstar"
    os.environ["PG_DB_USER"] = "postgres"
    os.environ["PG_DB_HOST"] = "localhost"
    os.environ["PG_DB_PW"] = "postgres"

    filepath = sys.argv[1]
    path_parts = filepath.split('/')
    local_path = path_parts[0]+'/'+path_parts[1]
    filename = "/"
    for part in path_parts[2:-1]:
        filename += part + '/'
    filename += path_parts[-1]

    if filepath[-6:] == '20.tar':
        LOGGER.info("translating min file " + filepath)
        translate_min_file(local_path, filename)

    elif filepath[-7:] == 'Day.tgz':
        LOGGER.info("translating day file " + filepath)
        translate_day_file(local_path, filename)

    elif filepath[-3:] == 'tgz':
        LOGGER.info("translating hour file " + filepath)
        translate_hour_file(local_path, filename)

    LOGGER.info("Process complete.")