#run this script to create and fill the tables in the database
import os
import psycopg2

dbname = os.getenv('PG_DB_NAME')
user = os.getenv('PG_DB_USER')
host = os.getenv('PG_DB_HOST')
password = os.getenv('PG_DB_PW')

# dbname = 'tstar'
# user = 'postgres'
# host = 'localhost'
# password = 'postgres'

conn = psycopg2.connect(dbname=dbname, user=user, host=host, password=password)
cur = conn.cursor()

#create gtsm_config table
print("Making gtsm_config table")
sql =   """
        CREATE TABLE gtsm_configs (
            fcid text,
            "16charid" text,
            download boolean,
            fqdn text,
            ip text,
            port int,
            username text,
            network_code text,
            key text,
            PRIMARY KEY (fcid)
        );
        
        INSERT INTO gtsm_configs (fcid, "16charid", download, fqdn, ip, port, username, network_code, key)
        VALUES ('TSM1', 'tstartsm1bit2021', false, 'tstartsm1bit2021.pbo-data-bsm.net', '185.220.53.10', 140, 'gtsm21', 'IV', 'tstar');
        
        INSERT INTO gtsm_configs (fcid, "16charid", download, fqdn, ip, port, username, network_code, key)
        VALUES ('TSM2', 'tstartsm2bit2021', true, 'tstartsm2bit2021.pbo-data-bsm.net', '185.220.53.11', 140, 'gtsm21', 'IV', 'tstar');
        
        INSERT INTO gtsm_configs (fcid, "16charid", download, fqdn, ip, port, username, network_code, key)
        VALUES ('TSM3', 'tstartsm3bit2021', true, 'tstartsm3bit2021.pbo-data-bsm.net', '185.220.53.13', 140, 'gtsm21', 'IV', 'tstar');
        
        INSERT INTO gtsm_configs (fcid, "16charid", download, fqdn, ip, port, username, network_code, key)
        VALUES ('TSM4', 'tstartsm4bit2022', false, 'tstartsm4bit2022.pbo-data-bsm.net', '', 140, 'gtsm21', 'IV', 'tstar');
        
        INSERT INTO gtsm_configs (fcid, "16charid", download, fqdn, ip, port, username, network_code, key)
        VALUES ('TSM5', 'tstartsm5bit2022', false, 'tstartsm5bit2022.pbo-data-bsm.net', '', 140, 'gtsm21', 'IV', 'tstar');
        
        INSERT INTO gtsm_configs (fcid, "16charid", download, fqdn, ip, port, username, network_code, key)
        VALUES ('TSM6', 'tstartsm6bit2022', false, 'tstartsm6bit2022.pbo-data-bsm.net', '', 140, 'gtsm21', 'IV', 'tstar');
        """

cur.execute(sql)
conn.commit()


#create dataflow_tracking table
print("Making dataflow_tracking table")
sql =   """
        CREATE TABLE dataflow_tracking (
            filename text,
            fcid text,
            downloaded boolean,
            download_time timestamp with time zone,
            mseed_conversion boolean,
            mseed_conversion_time timestamp with time zone,
            archived boolean,
            archive_time timestamp with time zone,
            file_type text,
            file_date date,
            PRIMARY KEY (filename)
        );
        """
cur.execute(sql)
conn.commit()

#create bsm_data_type table
print("Making bsm_data_type table")
sql =   """
        CREATE TABLE bsm_data_type (
            id integer,
            bsm_datafile_session_id integer,
            bottle_name text,
            description text,
            units text,
            units_abbr text,
            sampling_interval_seconds real,
            bottle_data_type text,
            missing_sample_value real,
            seed_channel text,
            seed_location text,
            file_type text,
            PRIMARY KEY (id)
        );
        """
cur.execute(sql)
conn.commit()

#load data into bsm_data_type from csv
csv_file_name = 'bsm_data_type.csv'
sql = "COPY bsm_data_type FROM STDIN DELIMITER ',' CSV HEADER"
cur.copy_expert(sql, open(csv_file_name, "r"))
conn.commit()
cur.close()
conn.close()

print("complete.")