# TABOO STAR GTSM Acquisition 
This repository contains an updated subset of the original PBO/UNAVCO bsmflow code that will be
installed at INGV and used to acquire the TABOO-STAR GTSM data.

M Gottlieb, March 2022

### System requirements:
- This code should be installed on a server capable of running hourly cron jobs
- Server must have docker installed 
- recommended to install pgAdmin or some other way to view database tables

### Overview
The hourly workflow of this code is as follows:
1. look up a list of stations to download, and for each station do the following:
2. connect to remote station, get a list of files on logger
3. compare that list to local dataflow table of files already downloaded
4. download any new files
5. if successful, mark as downloaded in dataflow table
6. translate the file to miniseed
7. if successful, mark as translated in dataflow table
8. generate some plots showing download history

### Installation and Usage:

1. install docker (or verify it is installed) on machine
    ```
    docker --version
    ```
2. clone the repo 

    ```
    git clone https://gitlab.com/earthscope/singleuser/gottlieb/tstar-gtsm-acquisition.git
    ``` 

2. make a directory called pgdata in this folder.  make this manually because it needs to have same owner/group as other files
    ```
    cd tstar-gtsm-acquisition
    mkdir pgdata
    ```
3. run 'run_pg_docker.sh' to start postgres container and mount volume
    - confirm container is running with ```docker ps``` 
    - wait > 30 secs to make sure database initialization completes
     
4. run 'run_create_tables.sh' to create and populate the database tables 
    - view output with:  ```docker logs create_tables```
    - confirm data exists by connecting via PgAdmin or equivalent
    - there should be 3 tables in database 'tstar'
        - 'gtsm_configs'
            - This table determines which stations are downloaded and how to connect to them    
        - 'dataflow_tracking'
            - This table should start empty, and be populated as files are downloaded
        - 'bsm_data_type'
            - This table contains the mapping of GTSM channel names to seed channel names, and is used for miniseed translation 

5. copy the contents of the data directory to /data, or wherever the data should be downloaded to, making sure write permissions are sufficient.  should be /data/TSM1/ etc.

5. initial run acquisition code

    ```./build_and_run_acquisition_docker.sh``` 
    - This will take a long time (many hours per station) on the initial run, as it will download the entire contents of each data logger
    - Set to run in background (-d in docker run), so to view output run: 
    ```       docker logs gtsm_hourly_acquisition```
    - Miniseed translation of the 20hz 'Min' files is about 10s per hour-long file on my laptop, this will be the slowest part by far
    - watch stderr/stdout for any warnings/errors, debug as necessary 
    - ensure files are downloading and translating into the expected locations in the data directory
    - once it looks like everything is working, let run until complete (may take all day)

6. set up cron to run the 'run_acquisition_docker.sh' script 1x per hour (a few minutes after the hour)
    - uses existing image from previous step, so dont need to rebuild every time
    - once local archive is built (previous step), this script should run quickly to just download the newest files (2 or 3 files per station per run)
    - most recent log is viewable with ```docker logs gtsm_hourly_acquisition```
    ```        
        5 * * * * cd ~/tstar-gtsm-acquisition/ && ./run_aqcuisition_docker.sh  
    ```
7. adding additional stations
    - update gtsm_config table with IP information when known, and set download to 'true' for any stations online

### Contents:

run_pg_docker.sh
- spins up a local postgresql database with station configs and download tables
- mounted to (initially empty) folder pgdata
- set to restart unless manually stopped
- can manually stop with 'docker stop postgres'

run_create_tables.sh
- builds and runs create_tables image 

Dockerfile_create_tables
- defines env for running create_tables script

create_tables.py
- python script that creates three tables and inputs inital config info and seed code mapping data

.keys/
- directory containing public/private key pair for accessing stations.

data/
- directory to store bottle and miniseed data and download history plots
 
requirements.txt
- list of python modules to install, with tested version numbers

hourly_acquisition.py
- script that will be run every hour on a cron and drives the other processes
- connects to local database
    - if station 'download' field is set to 'true', then
        - create an ssh connection to the station
        - download a list of all data files on logger at field station
        - download a list of files already downloaded from local database
        - open an ssh connection to field logger 
            - scp any files on logger not marked in database as downloaded
        - update database with new downloads
        - initiate miniseed translation of each successful download
        - after downloads are complete, make plots of file download history
            - note: currently this produces a warning about pandas sql compatibility and suggests implementing SQLAlchemy, but you can ignore this warning 

Dockerfile_hourly_acquisition
- debian linux container built on official python 3.9 image
- pip installs required modules
- copies in python scripts and keys directory
- executes hourly_acquisition.py script when run

run_acquisition_docker.sh
- wrapper script that runs the hourly acquisition container
- this should be set up on a cron to run a few minutes after each hour
- 'docker run' feeds in required environment variables to container

build_and_run_acquisition_docker.sh
- wrapper script that builds and runs the hourly acquisition container
- only need to use this for initial download or when making changes to the scripts
- 'docker run' feeds in required environment variables to container

miniseed_translator.py
- invoked after each successful bottle download
- unpacks the bottle tar/tgz files
- builds miniseed header from bottle filename, bottle file header, and bsm_data_type table
- builds miniseed file from header and bottle data
- merges one minute long 20hz miniseed files into hour long files 
    - this step is slow
- writes files to data/TSM[]/data/miniseed/
- marks as translated in the dataflow_tracking table
- This script can also be invoked directly from the tstar-gtsm-acquisition main directory if need to manually translate a file
```
    > python miniseed_translator.py path/to/local/file
```

miniseed_read.py
- a bit of example code to open miniseed files, will be slow with large number of files

### Notes on miniseed translation and archiving:

Each unpacked bottle file (1 channel/loc) goes into its own miniseed file.  Exception is that 20hz data is actually 60 one-minute bottles, and those are combined as a single hour-long miniseed file

File naming convention used is same as at UNAVCO: 
- bottlename.ms.CHA.LC
- ie. TSM20220010CH0_20.ms.BS1.T0, TSM22200100CH0.ms.LS1.T0, TSM222001CH0.ms.RS1.T0  

Limitations:
- This code package does not include seedlink to an archive, that will need to be set up separately and pointed at the miniseed folders
- The local miniseed 'archive' is a folder per station, which will quickly grow to a large number of files.  Miniseed file cleanup or organization is not included here.
- Bottle files are just stored locally as well, and should be backed up somewhere else as required.

### File download history plots

The code generates 2 plots for each station every run.  They are placed into the highest level directory for each station (ie /data/TSM2/)

One is a 10 day history (stacked bar chart) of total files downloaded.  There should be 1 low rate, 24 1-hz, and 24 20-hz files each day. 
This plot also includes the time of most recent successful connection to the station.

The second (lifetime_history) shows 3 scatter plots, with a point per day showing the percent complete for each file type.
  