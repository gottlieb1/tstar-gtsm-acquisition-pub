# run this to start local postgres database instance

echo "stopping existing containers"
docker stop postgres
echo "removing existing containers"
docker rm postgres

docker network create 'local_net'

docker run -p 5432:5432 -d \
    --network='local_net' \
    --user "$(id -u):$(id -g)" \
    -e POSTGRES_PASSWORD=postgres \
    -e POSTGRES_USER=postgres \
    -e POSTGRES_DB=tstar \
    --restart unless-stopped \
    --name postgres \
    -v /etc/passwd:/etc/passwd \
    -v ${PWD}/pgdata:/var/lib/postgresql/data \
    postgres


