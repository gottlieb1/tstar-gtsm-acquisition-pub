#run this script on an hourly cron
#use build_and_run_acquisition_docker.sh for initial run

echo "stopping existing containers"
docker stop gtsm_hourly_acquisition
echo "removing existing containers"
docker rm gtsm_hourly_acquisition

docker run \
    -d \
    --network='local_net' \
    -e PG_DB_HOST=postgres \
    -e PG_DB_NAME=tstar \
    -e PG_DB_USER=postgres \
    -e PG_DB_PW=postgres \
    -v /data:/app/data \
    --name gtsm_hourly_acquisition \
      hourly_acquisition

##use this command if you need to access the container directly via interactive terminal
#docker exec -it gtsm_hourly_acquisition /bin/bash