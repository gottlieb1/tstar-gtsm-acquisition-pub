echo "stopping existing containers"
docker stop create_tables
echo "removing existing containers"
docker rm create_tables


docker build -f Dockerfile_create_tables -t create_tables .

docker run \
    --network='local_net' \
    -e PG_DB_HOST=postgres \
    -e PG_DB_NAME=tstar \
    -e PG_DB_USER=postgres \
    -e PG_DB_PW=postgres \
    --name create_tables \
    create_tables
