#run this script for the initial download, or if changes have been made to codebase
#otherwise just use run_acquisition_docker.sh

docker stop gtsm_hourly_acquisition
docker rm gtsm_hourly_acquisition

docker image rm hourly_acquisition
docker build -f Dockerfile_hourly_acquisition -t hourly_acquisition .

docker run \
    -d \
    --network='local_net' \
    -e PG_DB_HOST=postgres \
    -e PG_DB_NAME=tstar \
    -e PG_DB_USER=postgres \
    -e PG_DB_PW=postgres \
    -v /data:/app/data \
    --name gtsm_hourly_acquisition \
      hourly_acquisition


#docker exec -it gtsm_hourly_acquisition /bin/bash